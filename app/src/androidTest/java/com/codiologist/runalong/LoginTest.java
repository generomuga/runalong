package com.codiologist.runalong;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityActivityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);

    @Test
    public void testInputEmail(){
        onView(withId(R.id.editTextEmail)).perform(typeText("generomuga12693@gmail.com"));
        //onView(withId(R.id.buttonLogin)).perform(click());
    }

    @Test
    public void testInputPassword(){
        onView(withId(R.id.editTextPassword)).perform(typeText("12345678"));
    }

}
