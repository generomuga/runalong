package com.codiologist.runalong;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.codiologist.runalong.adapter.CustomAdapterRunner;
import com.codiologist.runalong.dialog.DialogMatch;
import com.codiologist.runalong.object.UserDetail;
import com.facebook.login.LoginManager;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryDataEventListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.DoubleToLongFunction;

public class RunActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, OnMapReadyCallback {

    //location
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationManager mLocationManager;
    private boolean mGPSEnabled = false;
    private boolean mNetworkEnabled = false;

    //firebase
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    //geofire
    GeoFire mGeoRunnersAvailable;
    MapView mMapView;
    private GoogleMap mGoogleMap;

    //value
    private String mUid;
    private ArrayList<UserDetail> userDetails;

    //views
    private Button mLogout;
    private ImageButton mRefresh;
    private ListView mRunners;
    private TextView mTips;

    private Toolbar toolbar;

    public String[] name = {"gene", "Mica"};

    Context mContext;

    UserDetail userDetail;

    private String buddyName = "A";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //Toast.makeText(getApplicationContext(), "coes", Toast.LENGTH_LONG).show();
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                    }

                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
    }

    private void createMap() {

        mMapView = (MapView) findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);

        createMap();

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        //check gps
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                //Toast.makeText(getApplicationContext(), "Build 1", Toast.LENGTH_LONG).show();
            } else {
                checkLocationPermission();
            }
        } else {
            //check if gps is on
            if (!isLocationEnabled()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RunActivity.this);
                builder.setTitle("Enable Location Service")
                        .setMessage("Open Location")
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //buildGoogleApiClient();
                                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                buildGoogleApiClient();
                Toast.makeText(getApplicationContext(), "API built", Toast.LENGTH_SHORT).show();
            }
        }

        mContext = this;

        mTips = (TextView) findViewById(R.id.textTips);
        mTips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RunActivity.this, TipsActivity.class));
            }
        });

        //views
        //mLogout = (Button) findViewById(R.id.buttonLogout);
        //mRefresh = (ImageButton) findViewById(R.id.imageButtonRefresh);
        mRunners = (ListView) findViewById(R.id.listViewRunners);

        //toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbarRun);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("SIGN UP");
        //set for back button in the toolbar
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageButton backButton = (ImageButton) findViewById(R.id.imageButtonBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RunActivity.this, ProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        ImageButton logoutButton = (ImageButton) findViewById(R.id.imageButtonLogout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchUid();
                resetCoordinates();
                resetRequest();
                resetRun();
                //mAuth.signOut();
                //googleSignOut();
                startActivity(new Intent(RunActivity.this, LoginActivity.class));
            }
        });

        //firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        //value
        mUid = mAuth.getCurrentUser().getUid();
        userDetails = new ArrayList<>();

        /*mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLocation != null) {
                    getNearestRunners();
                    //DialogMatch dialogMatch = new DialogMatch(mContext);
                    //dialogMatch.show();

                }
            }
        });*/

        //reset run
        resetRun();
        checkMatch();
        //resetRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //resetRequest();
        //resetRun();
        //resetRunStatus();

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            //Toast.makeText(getApplicationContext(), "OFlle", Toast.LENGTH_SHORT).show();
            getNearestRunnersOffline();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        //check gps
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                Toast.makeText(getApplicationContext(), "Build 1", Toast.LENGTH_LONG).show();
            } else {
                checkLocationPermission();
            }
        } else {
            //check if gps is on
            if (!isLocationEnabled()){
                AlertDialog.Builder builder = new AlertDialog.Builder(RunActivity.this);
                builder.setTitle("Enable Location Service")
                        .setMessage("Open Location")
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //buildGoogleApiClient();
                                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
            else{
                buildGoogleApiClient();
            }

        }*/
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(RunActivity.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                                //buildGoogleApiClient();
                            }
                        })
                        .create()
                        .show();
                buildGoogleApiClient();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }


    }

    private void checkMatch() {
        DatabaseReference matchRef = mDatabase.child("userProfile").child(mUid);
        matchRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    Map<String, Object> mapMatch = (Map<String, Object>) dataSnapshot.getValue();
                    if (Integer.parseInt(mapMatch.get("isRunning").toString()) == 1) {
                        /*DialogMatch dialogMatch = new DialogMatch(mContext);
                        dialogMatch.setCancelable(true);
                        dialogMatch.show();*/
                        startActivity(new Intent(RunActivity.this, ChatActivity.class));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void getNearestRunners() {
        final CustomAdapterRunner customAdapterRunner = new CustomAdapterRunner(userDetails, getApplicationContext(), mLocation);
        //mRunners.setAdapter(customAdapterRunner);

        GeoQuery geoQuery = mGeoRunnersAvailable.queryAtLocation(new GeoLocation(mLocation.getLatitude(), mLocation.getLongitude()), ProfileActivity.radius);
        geoQuery.addGeoQueryDataEventListener(new GeoQueryDataEventListener() {
            @Override
            public void onDataEntered(DataSnapshot dataSnapshot, GeoLocation location) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    if (!mUid.equals(dataSnapshot.getKey())) {
                        userDetails.clear();

                        //Toast.makeText(getApplicationContext(), "GEne", Toast.LENGTH_SHORT).show();

                        DatabaseReference userProfile = mDatabase.child("userProfile").child(dataSnapshot.getKey());
                        userProfile.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                userDetail = dataSnapshot.getValue(UserDetail.class);
                                userDetails.add(userDetail);
                                mRunners.deferNotifyDataSetChanged();
                                mRunners.setAdapter(customAdapterRunner);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onDataExited(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onDataMoved(DataSnapshot dataSnapshot, GeoLocation location) {

            }

            @Override
            public void onDataChanged(DataSnapshot dataSnapshot, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    private void getNearestRunnersOffline() {
        GeoQuery geoQuery = mGeoRunnersAvailable.queryAtLocation(new GeoLocation(mLocation.getLatitude(), mLocation.getLongitude()), ProfileActivity.radius);
        geoQuery.addGeoQueryDataEventListener(new GeoQueryDataEventListener() {
            @Override
            public void onDataEntered(DataSnapshot dataSnapshot, GeoLocation location) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    if (mUid != null) {
                        if (!mUid.equals(dataSnapshot.getKey())) {
                            //Toast.makeText(getApplicationContext(), "GEne", Toast.LENGTH_SHORT).show();
                            pushNotification();
                            notifySound();
                        }
                    }

                }
            }

            @Override
            public void onDataExited(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onDataMoved(DataSnapshot dataSnapshot, GeoLocation location) {

            }

            @Override
            public void onDataChanged(DataSnapshot dataSnapshot, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    private void pushNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.refresh_button)
                        .setContentTitle("FitChum ALERT!")
                        .setContentText("Possible nearest buddy found!");

        Intent notificationIntent = new Intent(this, RunActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    private void notifySound() {
        try {

            AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            switch (audio.getRingerMode()) {
                case AudioManager.RINGER_MODE_NORMAL:
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                    break;
                case AudioManager.RINGER_MODE_SILENT:
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(500);
                    break;
                case AudioManager.RINGER_MODE_VIBRATE:
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateLocationCoordinates() {
        resetTypeLevel(ProfileActivity.tempType);
        //resetTypeLevel("1");
        DatabaseReference runnersAvailable = mDatabase.child("runnersAvailable").child(Integer.toString(ProfileActivity.mType));
        mGeoRunnersAvailable = new GeoFire(runnersAvailable);

        double lat = mLocation.getLatitude();
        double lon = mLocation.getLongitude();

        /*if (lat == 0 && lon == 0){
            lat = 0.001;
            lon = 0.01;
        }*/

        mGeoRunnersAvailable.setLocation(mUid, new GeoLocation(lat, lon), new GeoFire.CompletionListener() {
            @Override
            public void onComplete(String key, DatabaseError error) {
                //Toast.makeText(getApplicationContext(), key, Toast.LENGTH_LONG).show();
            }
        });

    }

    private void resetTypeLevel(String type) {
        final DatabaseReference requestRef = mDatabase.child("runnersAvailable").child(type).child(mUid);
        requestRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            mLocation = location;
            //mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Me"));
            CameraPosition cameraPosition = CameraPosition.builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(15).bearing(0).tilt(45).build();
            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            updateLocationCoordinates();
            getNearestRunners();
            //getNearestRunnersOffline();
            locateBuddy();
        } else {
            Toast.makeText(getApplicationContext(), "null location", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void resetRun() {
        int mSelectResId = getApplication().getResources().getIdentifier("select_button", "drawable", "com.codiologist.runalong");


        HashMap mapUserProfile = new HashMap();
        mapUserProfile.put("hasRequest", 0);
        mapUserProfile.put("isRunning", 0);
        mapUserProfile.put("imageRes", mSelectResId);

        if (mAuth.getCurrentUser() != null) {
            DatabaseReference myProfile = mDatabase.child("userProfile").child(mUid);
            myProfile.updateChildren(mapUserProfile).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {

                }
            });
        }
    }

    private void resetRequest() {
        final DatabaseReference requestRef = mDatabase.child("userRequest").child(mUid);
        requestRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //resetImageRes(mUid);
                mAuth.signOut();
                googleSignOut();
                LoginManager.getInstance().logOut();
            }
        });
    }

    private void resetRequestOU(final String uid) {
        final String temp_uid = mUid;
        resetImageRes(uid);
        if (!TextUtils.isEmpty(temp_uid)) {
            //Toast.makeText(getApplicationContext(), temp_uid, Toast.LENGTH_SHORT).show();
            final DatabaseReference requestRef = mDatabase.child("userRequest").child(uid).child(temp_uid);
            requestRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    mAuth.signOut();
                    googleSignOut();
                    LoginManager.getInstance().logOut();
                }
            });
        }
    }

    private void resetImageRes(String uid) {
        DatabaseReference resetRunRef = mDatabase.child("userProfile").child(uid);
        HashMap mapResetRun = new HashMap();
        mapResetRun.put("imageRes", 2131099828);
        resetRunRef.updateChildren(mapResetRun).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {

            }
        });
    }

    private void searchUid() {
        DatabaseReference searchRef = mDatabase.child("userRequest");
        searchRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zoneSnapshot : dataSnapshot.getChildren()) {
                    resetRequestOU(zoneSnapshot.getKey());
                    //Toast.makeText(getApplicationContext(), zoneSnapshot.getKey(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void resetRunStatus() {
        DatabaseReference resetRunRef = mDatabase.child("userProfile").child(mUid);
        HashMap mapResetRun = new HashMap();
        mapResetRun.put("isRunning", 0);
        resetRunRef.updateChildren(mapResetRun);
    }

    private void resetCoordinates() {
        DatabaseReference runnersAvailable0 = mDatabase.child("runnersAvailable").child("0").child(mUid);
        runnersAvailable0.removeValue();
        DatabaseReference runnersAvailable1 = mDatabase.child("runnersAvailable").child("1").child(mUid);
        runnersAvailable1.removeValue();
        DatabaseReference runnersAvailable2 = mDatabase.child("runnersAvailable").child("2").child(mUid);
        runnersAvailable2.removeValue();
    }

    @Override
    public void onBackPressed() {
        //this.moveTaskToBack(true);
        startActivity(new Intent(RunActivity.this, ProfileActivity.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            //Toast.makeText(getApplicationContext(), "OFlle", Toast.LENGTH_SHORT).show();
            getNearestRunnersOffline();
        }
    }

    private void googleSignOut() {
        mAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {

            }
        });
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                //Intent intent = new Intent(RunActivity.this, ProfileActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(new Intent(RunActivity.this, ProfileActivity.class));
                //finish();
                //return true;
                break;
            case R.id.menu_logout:
                resetRequest();
                mAuth.signOut();
                googleSignOut();
                startActivity(new Intent(RunActivity.this, LoginActivity.class));
                break;
                //return true;
            default:
                return super.onOptionsItemSelected(item);

        }
        return super.onOptionsItemSelected(item);
    }*/

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_run,menu);
        return true;
    }*/

    protected boolean isLocationEnabled() {
        String le = Context.LOCATION_SERVICE;
        mLocationManager = (LocationManager) getSystemService(le);
        if (!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getApplicationContext());
        mGoogleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    private void locateBuddy(){
        DatabaseReference mBuddy = mDatabase.child("runnersAvailable").child(ProfileActivity.tempType);
        mBuddy.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mGoogleMap.clear();
                for (DataSnapshot buddyInfo: dataSnapshot.getChildren()){
                    String uid = buddyInfo.getKey();
                    if (!uid.equals(mUid)) {
                        String lat = buddyInfo.child("l").child("0").getValue().toString();
                        String lon = buddyInfo.child("l").child("1").getValue().toString();
                        //Toast.makeText(getApplicationContext(), lat+' '+lon, Toast.LENGTH_SHORT).show();
                        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon))).title(""));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /*private String getBuddyName(String uid) {
        DatabaseReference mProfile = mDatabase.child("userProfile").child(uid);
        mProfile.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (!TextUtils.isEmpty(dataSnapshot.child("name").getValue().toString())) {
                        Toast.makeText(getApplicationContext(), dataSnapshot.child("name").getValue().toString(), Toast.LENGTH_SHORT).show();
                        buddyName = dataSnapshot.child("name").getValue().toString();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return buddyName;
    }*/

}
