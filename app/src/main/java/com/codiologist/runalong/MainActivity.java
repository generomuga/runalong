package com.codiologist.runalong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    //firebase
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //firebase
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        //check if user is authenticated
        if (mAuth.getCurrentUser() != null){
            if (user != null) {
                if (user.isEmailVerified()) {
                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                }
                else{
                    mAuth.signOut();
                    Toast.makeText(getApplicationContext(), "Please verify your account!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            }
        }
        else{
            startActivity(new Intent(MainActivity.this, InfoActivity.class));
        }

    }
}
