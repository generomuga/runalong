package com.codiologist.runalong;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.codiologist.runalong.object.UserDetail;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    //views
    private Button mContinue;
    private RadioGroup mLevelGroup;
    private RadioButton mBeginner;
    private RadioButton mIntermediate;
    private RadioButton mAdvance;
    private RadioButton mSprint;
    private RadioButton m5k;
    private RadioButton mMarathon;
    private EditText mRadius;

    //firebase
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public static int mType = 1;
    public static int mLevel = 1;

    private String mUid;

    private Toolbar toolbar;

    public static String tempType="0";

    public static float radius;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        prefs = this.getSharedPreferences(
                "com.codiologist.runalong", Context.MODE_PRIVATE);
        editor = prefs.edit();

        //firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mUid = mAuth.getCurrentUser().getUid();

        //init views
        mContinue = (Button) findViewById(R.id.buttonContinue);
        mBeginner = (RadioButton) findViewById(R.id.radioButtonBeginner);
        mIntermediate = (RadioButton) findViewById(R.id.radioButtonIntermediate);
        mAdvance = (RadioButton) findViewById(R.id.radioButtonAdvance);
        mSprint = (RadioButton) findViewById(R.id.radioButtonSprint);
        m5k = (RadioButton) findViewById(R.id.radioButton5k);
        mMarathon = (RadioButton) findViewById(R.id.radioButtonMarathon);
        mRadius = (EditText) findViewById(R.id.editRadius);
        //mLevelGroup = (RadioGroup) findViewById(R.id.rgLevel);

        //set radius
        float rradius = prefs.getFloat("radius", 1);
        mRadius.setText(String.valueOf(rradius));

        //toolbar
        //toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbarProfile);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("SIGN UP");
        //set for back button in the toolbar
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageButton backButton = (ImageButton) findViewById(R.id.imageButtonBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, RunActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        //load
        loadLevelType();

        //check
        checkLevel();
        checkType();

        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadius();
                setLevel();
                setType();
                sendLevel();
                startActivity(new Intent(ProfileActivity.this, RunActivity.class));
            }
        });
    }

    private void setRadius(){
        if (!TextUtils.isEmpty(mRadius.getText())){
            try {
                radius = Float.parseFloat(mRadius.getText().toString());
                editor.putFloat("radius", radius);
                editor.apply();
            }
            catch (Exception e){
                mRadius.setText("1.0");
            }
        }
        else{
            radius = 1;
        }
    }

    private void sendLevel(){
        DatabaseReference userProfile = mDatabase.child("userProfile").child(mUid);
        HashMap mapPutInfo = new HashMap();
        mapPutInfo.put("level",mLevel);
        mapPutInfo.put("type", mType);
        userProfile.updateChildren(mapPutInfo);
    }

    private void setLevel(){
       if (mBeginner.isChecked()){
           mLevel = 0;
       }
       if (mIntermediate.isChecked()){
           mLevel = 1;
       }
       if (mAdvance.isChecked()){
           mLevel = 2;
       }
    }

    private void setType(){
        if (mSprint.isChecked()){
            mType = 0;
        }
        if (m5k.isChecked()){
            mType = 1;
        }
        if(mMarathon.isChecked()){
            mType = 2;
        }
    }

    private void loadLevelType(){
        DatabaseReference userProfile = mDatabase.child("userProfile").child(mUid);
        userProfile.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0){
                    //Map<String, Object> mapUserProfile = (Map<String, Object>) dataSnapshot.getValue();
                    UserDetail userDetail = dataSnapshot.getValue(UserDetail.class);
                    tempType = Integer.toString(userDetail.getType());
                    switch (userDetail.getLevel()){
                        case 0:
                            mBeginner.setChecked(true);
                            mIntermediate.setChecked(false);
                            mAdvance.setChecked(false);
                            break;
                        case 1:
                            mIntermediate.setChecked(true);
                            mBeginner.setChecked(false);
                            mAdvance.setChecked(false);
                            break;
                        case 2:
                            mAdvance.setChecked(true);
                            mBeginner.setChecked(false);
                            mIntermediate.setChecked(false);
                            break;
                    }
                    switch (userDetail.getType()){
                        case 0:
                            mSprint.setChecked(true);
                            m5k.setChecked(false);
                            mMarathon.setChecked(false);
                            break;
                        case 1:
                            m5k.setChecked(true);
                            mSprint.setChecked(false);
                            mMarathon.setChecked(false);
                            break;
                        case 2:
                            mMarathon.setChecked(true);
                            m5k.setChecked(false);
                            mSprint.setChecked(false);
                            break;
                    }
                }
                else{
                    mBeginner.setChecked(true);
                    mIntermediate.setChecked(false);
                    mAdvance.setChecked(false);

                    mSprint.setChecked(true);
                    m5k.setChecked(false);
                    mMarathon.setChecked(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void checkLevel(){
        mBeginner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBeginner.setChecked(true);
                mIntermediate.setChecked(false);
                mAdvance.setChecked(false);
            }
        });

        mIntermediate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIntermediate.setChecked(true);
                mBeginner.setChecked(false);
                mAdvance.setChecked(false);
            }
        });

        mAdvance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdvance.setChecked(true);
                mBeginner.setChecked(false);
                mIntermediate.setChecked(false);
            }
        });
    }

    private void checkType(){
        mSprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSprint.setChecked(true);
                m5k.setChecked(false);
                mMarathon.setChecked(false);
            }
        });

        m5k.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m5k.setChecked(true);
                mSprint.setChecked(false);
                mMarathon.setChecked(false);
            }
        });

        mMarathon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMarathon.setChecked(true);
                m5k.setChecked(false);
                mSprint.setChecked(false);
            }
        });
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(ProfileActivity.this, RunActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ProfileActivity.this, RunActivity.class));
    }
}
