package com.codiologist.runalong.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.codiologist.runalong.R;

public class DialogMatch extends Dialog {

    Context context;

    public DialogMatch(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_match);
    }
}
