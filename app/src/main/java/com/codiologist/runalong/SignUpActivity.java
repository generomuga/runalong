package com.codiologist.runalong;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.codiologist.runalong.object.UserDetail;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {

    //views
    private Button mSignUp;
    private EditText mName;
    private EditText mEmail;
    private EditText mPassword;
    private EditText mCPassword;
    private ProgressDialog mProgress;
    private TextView mLogin;

    //firebase
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    //toolbar
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //firebae
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        //views
        mSignUp = (Button) findViewById(R.id.buttonRegister);
        mName = (EditText) findViewById(R.id.editTextName);
        mEmail = (EditText) findViewById(R.id.editTextEmail);
        mPassword = (EditText) findViewById(R.id.editTextPassword);
        mCPassword = (EditText) findViewById(R.id.editTextConfirmPassword);
        mLogin = (TextView) findViewById(R.id.textViewLogin);

        //toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbarSignUp);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("SIGN UP");
        //set for back button in the toolbar
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageButton backButton = (ImageButton) findViewById(R.id.imageButtonBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        mProgress = new ProgressDialog(this);

        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });

    }

    private void signUp(){
        mProgress.setTitle("Signing up");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.show();

        final String name = mName.getText().toString().trim();
        final String email = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        String cpassword = mCPassword.getText().toString().trim();

        if (TextUtils.isEmpty(name)){
            mName.setError("Required field!");
            mProgress.dismiss();
            return;
        }
        if (TextUtils.isEmpty(email)){
            mEmail.setError("Required field!");
            mProgress.dismiss();
            return;
        }
        if (!email.contains("@")){
            mEmail.setError("Invalid email!");
            mProgress.dismiss();
            return;
        }
        if (TextUtils.isEmpty(password)){
            mPassword.setError("Required field!");
            mProgress.dismiss();
            return;
        }
        if (TextUtils.isEmpty(cpassword)){
            mCPassword.setError("Required field!");
            mProgress.dismiss();
            return;
        }
        if (!password.equals(cpassword)){
            mCPassword.setError("Password should be equal to confirm password!");
            mProgress.dismiss();
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()){
                    mProgress.dismiss();
                    Toast.makeText(getApplicationContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                }
                else{
                    final FirebaseUser user = mAuth.getCurrentUser();
                    final String uid = mAuth.getCurrentUser().getUid();

                    int mSelectResId = getApplication().getResources().getIdentifier("select_button", "drawable", "com.codiologist.runalong");
                    //Log.i("idP", Integer.toString(mSelectResId));

                    //instance of an object
                    final UserDetail userDetail = new UserDetail();
                    userDetail.setUid(uid);
                    userDetail.setName(name);
                    userDetail.setEmail(email.toLowerCase());
                    userDetail.setHasRequest(0);
                    userDetail.setLevel(0);
                    userDetail.setType(0);
                    userDetail.setImageRes(mSelectResId);
                    userDetail.setIsRunning(0);

                    user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (!task.isSuccessful()) {
                                mProgress.dismiss();
                                Toast.makeText(getApplicationContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                            }
                            else {
                                DatabaseReference userProfile = mDatabase.child("userProfile").child(uid);
                                userProfile.setValue(userDetail);

                                mProgress.dismiss();
                                Toast.makeText(getApplicationContext(), "Please verify your account in email", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                            }
                        }
                    });
                }
            }
        });


    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}
