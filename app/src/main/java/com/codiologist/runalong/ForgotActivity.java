package com.codiologist.runalong;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotActivity extends AppCompatActivity {

    private EditText mEmail;
    private Button mReset;
    private ProgressDialog mProgressDialog;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        mAuth = FirebaseAuth.getInstance();

        //initialize views
        mEmail = (EditText) findViewById(R.id.editTextEmail);
        mReset = (Button) findViewById(R.id.buttonReset);
        mProgressDialog = new ProgressDialog(this);

        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString();
                if (TextUtils.isEmpty(email)){
                    mEmail.setError("Email required!");
                }
                else {
                    reset(email);
                }
            }
        });
    }

    private void reset(String email){
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Resetting password...");
        mProgressDialog.show();
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(!task.isSuccessful()){
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Please check your email!", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            }
        });
    }

}
