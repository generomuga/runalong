package com.codiologist.runalong;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.codiologist.runalong.adapter.ViewPagerAdapter;

public class InfoActivity extends AppCompatActivity {

    private ViewPager mPager;
    private int[] layouts = {R.layout.info1,R.layout.info2, R.layout.info3, R.layout.info4, R.layout.info5, R.layout.info6};
    private ViewPagerAdapter mViewPagerAdapter;

    private LinearLayout dotsLayout;
    private ImageView[] dots;
    private Button mGetStarted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        if(Build.VERSION.SDK_INT >= 19){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        else{
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPagerAdapter = new ViewPagerAdapter(layouts,this);
        mPager.setAdapter(mViewPagerAdapter);

        mGetStarted = (Button) findViewById(R.id.buttonGetStarted);

        dotsLayout = (LinearLayout) findViewById(R.id.dotsLayout);
        createDots(0);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                createDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mGetStarted.setTransformationMethod(null);
        mGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InfoActivity.this,LoginActivity.class));
                finish();
            }
        });

    }

    private void createDots(int currentPosition){
        if(dotsLayout!=null){
            dotsLayout.removeAllViews();
        }

        dots = new ImageView[layouts.length];

        for(int i=0;i<layouts.length;i++){
            dots[i] = new ImageView(this);
            if(i==currentPosition){
                dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.active_dots));
            }
            else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.default_dots));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            params.setMargins(11,0,11,0);

            dotsLayout.addView(dots[i],params);
        }
    }
}
