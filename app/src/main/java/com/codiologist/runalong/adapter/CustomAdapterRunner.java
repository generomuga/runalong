package com.codiologist.runalong.adapter;


import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.codiologist.runalong.R;
import com.codiologist.runalong.object.UserDetail;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CustomAdapterRunner extends ArrayAdapter<UserDetail>{

    private ArrayList<UserDetail> dataSet;
    Context mContext;
    Location location;
    float distance;
    DatabaseReference mDatabase;
    FirebaseAuth mAuth;
    String mUid;
    Boolean hasRequest;
    Boolean hasAccept;
    int mImageRequestId;

    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView txtLevel;
        TextView txtDistance;
        ImageView imageViewProfile;
        ImageButton imageButtonRequest;
    }

    public CustomAdapterRunner(ArrayList<UserDetail> data, Context context, Location location) {
        super(context, R.layout.list_runners, data);
        this.dataSet = data;
        this.mContext=context;
        this.location = location;

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mUid = mAuth.getCurrentUser().getUid();
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        distance = 0;
        hasRequest = false;
        hasAccept = false;

        final UserDetail userDetail = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_runners, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.textViewName);
            viewHolder.txtLevel = (TextView) convertView.findViewById(R.id.textViewLevel);
            viewHolder.txtType = (TextView) convertView.findViewById(R.id.textViewType);
            viewHolder.txtDistance = (TextView) convertView.findViewById(R.id.textViewDistance);
            viewHolder.imageButtonRequest = (ImageButton) convertView.findViewById(R.id.imageButtonRequest);

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        mImageRequestId = mContext.getResources().getIdentifier("pending_button", "drawable", "com.codiologist.runalong");
        Log.i("idP", Integer.toString(mImageRequestId));

        //check if current user has send request
        /*DatabaseReference sendRequestRef = mDatabase.child("userProfile").child(mUid);
        sendRequestRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0){
                    Map<String, Object> mapHasRequest = (Map<String, Object>) dataSnapshot.getValue();
                    int hasRequest = Integer.parseInt(mapHasRequest.get("hasRequest").toString());
                    if (hasRequest == 1){
                        viewHolder.imageButtonRequest.setImageResource(R.drawable.selected_button);
                    }
                    else{
                        viewHolder.imageButtonRequest.setImageResource(R.drawable.select_button);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });*/


        //load image based on imageRes
        /*DatabaseReference imageResId = mDatabase.child("userProfile").child(userDetail.getUid());
        imageResId.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0){
                    Map<String, Object> mapImageRes = (Map<String, Object>) dataSnapshot.getValue();
                    int id = Integer.parseInt(mapImageRes.get("imageRes").toString());
                    viewHolder.imageButtonRequest.setImageResource(id);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/

        /*if (userDetail.getImageRes() == 2131099778) {
            //viewHolder.imageButtonRequest.setImageResource(userDetail.getImageRes());
            viewHolder.imageButtonRequest.setImageResource(R.drawable.select_button);
        }
        if (userDetail.getImageRes() == 2131099779) {
            //viewHolder.imageButtonRequest.setImageResource(userDetail.getImageRes());
            viewHolder.imageButtonRequest.setImageResource(R.drawable.selected_button);
        }
        if (userDetail.getImageRes() == 2131099776) {
            //viewHolder.imageButtonRequest.setImageResource(userDetail.getImageRes());
            viewHolder.imageButtonRequest.setImageResource(R.drawable.pending_button);
        }*/

        viewHolder.imageButtonRequest.setImageResource(userDetail.getImageRes());
        //viewHolder.imageButtonRequest.setImageResource(R.drawable.select_button);
        //Log.i("imageRes", Integer.toString(userDetail.getImageRes()));

        //query request
        final DatabaseReference userRequestRef = mDatabase.child("userRequest").child(mUid).child(userDetail.getUid());
        userRequestRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0){
                    Map<String, Object> mapAccepted = (Map<String, Object>) dataSnapshot.getValue();
                    hasRequest = true;

                    //checks if the request is accepted
                    if (Integer.parseInt(mapAccepted.get("run").toString()) == 0){
                        hasAccept = false;
                        //viewHolder.imageButtonRequest.setImageResource(R.drawable.pending_button);
                    }
                    else{
                        hasAccept = true;
                        //new
                        userRequestRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                            }
                        });
                    }

                    if (!hasAccept) {
                    }
                    else{
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        viewHolder.txtName.setText(userDetail.getName());

        String levelValue = "";
        String levelType = "";

        switch (userDetail.getLevel()){
            case 0:
                levelValue = "Beginner";
                break;
            case 1:
                levelValue = "Intermediate";
                break;
            case 2:
                levelValue = "Advance";
                break;
        }

        switch (userDetail.getType()){
            case 0:
                levelType = "Jogging";
                break;
            case 1:
                levelType = "Zumba";
                break;
            case 2:
                levelType = "Gym";
                break;
        }

        viewHolder.txtLevel.setText(levelValue);
        viewHolder.txtType.setText(levelType);

        //calculate distance
        //DatabaseReference nearbyRef = mDatabase.child("runnersAvailable").child(userDetail.getUid());
        DatabaseReference nearbyRef = mDatabase.child("runnersAvailable").child(levelValue).child(levelType).child(userDetail.getUid());

        nearbyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0){
                    Map<String, Object> userCoordinates = (Map<String, Object>) dataSnapshot.getValue();
                    String coordinates = userCoordinates.get("l").toString().trim();
                    String arrayCoordinates[] = coordinates.split(",");
                    Double latitude = Double.parseDouble(arrayCoordinates[0].replace("[",""));
                    Double longitude = Double.parseDouble(arrayCoordinates[1].replace("]",""));

                    //my location
                    Location locationMe = new Location("A");
                    locationMe.setLatitude(location.getLatitude());
                    locationMe.setLongitude(location.getLongitude());

                    //your location
                    Location locationNearBy = new Location("B");
                    locationNearBy.setLatitude(latitude);
                    locationNearBy.setLongitude(longitude);

                    distance = locationMe.distanceTo(locationNearBy);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //convert meters to km
        float km = (float) (distance * 0.001);

        //request for run
        viewHolder.txtDistance.setText(Float.toString((float) round(km,1))+" km");
        viewHolder.imageButtonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap mapUserRequest = new HashMap();
                HashMap mapSendRequest = new HashMap();
                HashMap mapImageRes = new HashMap();

                if (!hasRequest) {
                    DatabaseReference userRequestRef = mDatabase.child("userRequest").child(userDetail.getUid()).child(mUid);
                    DatabaseReference sendRequestRef = mDatabase.child("userProfile").child(mUid);
                    DatabaseReference imageResRef = mDatabase.child("userProfile").child(userDetail.getUid());

                    int resSelected = mContext.getResources().getIdentifier("selected_button", "drawable", "com.codiologist.runalong");
                    int resPending = mContext.getResources().getIdentifier("pending_button", "drawable", "com.codiologist.runalong");

                    mapUserRequest.put("run", 0);
                    mapSendRequest.put("hasRequest", 1);
                    mapSendRequest.put("imageRes", resPending);

                    //send
                    mapImageRes.put("imageRes", resSelected);

                    viewHolder.imageButtonRequest.setImageResource(resSelected);
                    //viewHolder.buttonRequest.setText("Reted");
                    userRequestRef.updateChildren(mapUserRequest).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (!task.isSuccessful()) {
                                //Toast.makeText(getContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                            } else {

                            }
                        }
                    });

                    sendRequestRef.updateChildren(mapSendRequest).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (!task.isSuccessful()) {
                                //Toast.makeText(getContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                            } else {
                                //Toast.makeText(getContext(), "Success", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    imageResRef.updateChildren(mapImageRes).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (!task.isSuccessful()) {
                                //Toast.makeText(getContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                            } else {
                                //Toast.makeText(getContext(), "Success", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }
                else {
                    HashMap mapIsRunning = new HashMap();

                    final DatabaseReference userAcceptRef = mDatabase.child("userRequest").child(mUid).child(userDetail.getUid());
                    DatabaseReference meIsRunningRef = mDatabase.child("userProfile").child(mUid);
                    DatabaseReference otherIsRunningRef = mDatabase.child("userProfile").child(userDetail.getUid());

                    mapUserRequest.put("run", 1);
                    mapIsRunning.put("isRunning", 1);

                    userAcceptRef.updateChildren(mapUserRequest).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (!task.isSuccessful()) {
                                //Toast.makeText(getContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                            } else {
                                // Toast.makeText(getContext(), "Success Accept 1", Toast.LENGTH_LONG).show();
                                //remove value after accept
                                //userAcceptRef.removeValue();
                            }
                        }
                    });

                    meIsRunningRef.updateChildren(mapIsRunning).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (!task.isSuccessful()) {
                                //Toast.makeText(getContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                            } else {
                                // Toast.makeText(getContext(), "Success Accept 1", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    otherIsRunningRef.updateChildren(mapIsRunning).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (!task.isSuccessful()) {
                                //Toast.makeText(getContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                            } else {
                                // Toast.makeText(getContext(), "Success Accept 1", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }

        });

        return convertView;
    }

    private double round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

}