package com.codiologist.runalong;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.codiologist.runalong.object.Chat;
import com.codiologist.runalong.object.UserDetail;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {

    //firebase
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private String mUid;

    //views
    private ListView mChat;
    private Button mSend;
    private Button mQuit;
    private EditText mMessage;
    private TextView mChatMessage;

    String message;
    String name;
    private String temp_key;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //init firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mUid = mAuth.getCurrentUser().getUid();

        //getCurrentName(mUid);
        acceptRequest();

        //views
        //mChat = (ListView) findViewById(R.id.listViewChat);
        mChatMessage = (TextView) findViewById(R.id.textViewChat);
        mChatMessage.setMovementMethod(new ScrollingMovementMethod());
        mMessage = (EditText) findViewById(R.id.editMessage);
        mQuit = (Button) findViewById(R.id.buttonQuit);
        mSend = (Button) findViewById(R.id.buttonSend);

        final DatabaseReference dbChat = mDatabase.child("chat").child("run1");
        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> map = new HashMap<String, Object>();
                temp_key = dbChat.push().getKey();
                dbChat.updateChildren(map);

                DatabaseReference message_root = dbChat.child(temp_key);
                Map<String, Object> map2 = new HashMap<String, Object>();
                map2.put("name", mAuth.getCurrentUser().getDisplayName());
                map2.put("msg", mMessage.getText().toString());
                message_root.updateChildren(map2);
                mMessage.setText(null);

                //mChatMessage.append(mMessage.getText().toString()+"\n");
            }
        });

        mQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Map<String, Object> map = new HashMap<String, Object>();
                temp_key = dbChat.push().getKey();
                dbChat.updateChildren(map);

                DatabaseReference message_root = dbChat.child(temp_key);
                Map<String, Object> map2 = new HashMap<String, Object>();
                map2.put("name", mAuth.getCurrentUser().getDisplayName());
                map2.put("msg", "quit in chat");
                message_root.updateChildren(map2);
                mMessage.setText(null);

                removeChat();
            }
        });


        dbChat.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                append_chat_conversation(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                append_chat_conversation(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //getMessages();
    }

    private void removeChat(){
        final DatabaseReference chatRef = mDatabase.child("chat").child("run1");
        chatRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                resetRun();
            }
        });
    }

    private void resetRun(){
        int mSelectResId = getApplication().getResources().getIdentifier("select_button", "drawable", "com.codiologist.runalong");


        HashMap mapUserProfile = new HashMap();
        mapUserProfile.put("hasRequest", 0);
        mapUserProfile.put("isRunning", 0);
        mapUserProfile.put("imageRes", mSelectResId);

        if (mAuth.getCurrentUser() != null) {
            DatabaseReference myProfile = mDatabase.child("userProfile").child(mUid);
            myProfile.updateChildren(mapUserProfile).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    startActivity(new Intent(ChatActivity.this, RunActivity.class));
                }
            });
        }
    }

    private String chat_msg, chat_user_name;
    private void append_chat_conversation(DataSnapshot dataSnapshot){

        Iterator i = dataSnapshot.getChildren().iterator();
        while (i.hasNext()){
            chat_msg = (String) ((DataSnapshot) i.next()).getValue();
            chat_user_name = (String) ((DataSnapshot) i.next()).getValue();

            mChatMessage.append(chat_user_name+" : "+chat_msg+"\n");
        }
    }

    private void acceptRequest(){
        final DatabaseReference dbChat = mDatabase.child("chat").child("run1");
        Map<String, Object> map = new HashMap<String, Object>();
        temp_key = dbChat.push().getKey();
        dbChat.updateChildren(map);
        DatabaseReference message_root = dbChat.child(temp_key);
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("name", mAuth.getCurrentUser().getDisplayName());
        map2.put("msg", " is your buddy.");
        message_root.updateChildren(map2);
    }

    private void getCurrentName(final String uid){
        DatabaseReference userProfile = mDatabase.child("userProfile").child(uid);
        userProfile.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetail userDetail = dataSnapshot.getValue(UserDetail.class);
                sendAcceptedRequest(uid, "run1", userDetail.getName(), userDetail.getName()+" is your buddy.");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendAcceptedRequest(String uid, String threadName, String name, String message){
        Chat chat = new Chat();
        chat.setmUid(uid);
        chat.setName(name);
        chat.setThreadName(threadName);
        chat.setMessage(message);

        DatabaseReference dbChat = mDatabase.child("chat").child(threadName).push();
        dbChat.setValue(chat);
    }

    private void getMessages(){
        final DatabaseReference chat = mDatabase.child("chat").child("run1");
        final CustomAdapter customAdapter = new CustomAdapter();
        ArrayList<String> nameList = new ArrayList<>();
        chat.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot val : dataSnapshot.getChildren()) {
                    Chat chat1 = val.getValue(Chat.class);
                    Toast.makeText(getApplicationContext(), chat1.getName(), Toast.LENGTH_SHORT).show();
                    //Chat chat1 = dataSnapshot.getValue(Chat.class);
                    name = chat1.getName();
                    message = chat1.getMessage();
                    mChat.setAdapter(customAdapter);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        customAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        removeChat();
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.layout_message, null);
            TextView textViewName = (TextView)    view.findViewById(R.id.textViewName);
            TextView textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
            textViewName.setText(name);
            textViewMessage.setText(message);
            return view;
        }
    }

}
